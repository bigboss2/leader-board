export const environment = {
  production: true,
  ASGARD_BASE_URL: 'https://asgard-consumer.vercel.app',
};
