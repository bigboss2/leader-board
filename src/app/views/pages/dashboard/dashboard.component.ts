import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import currencyFormatter from 'currency-formatter';
import * as moment from 'moment';
import {PoolRank} from '../../../core/model/PoolRank';
import {ImageNameService} from '../../../image-name.service';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  preserveWhitespaces: true
})
export class DashboardComponent implements OnInit {

  private gridApi;
  private gridColumnApi;

  constructor(private http: HttpClient, public imageServer: ImageNameService) {}
  title = 'Ranking';
  // @ts-ignore
  columnDefs = [
    {
      headerName: 'Pool',
      sortable: true, unSortIcon: true,
      filter: true,
      // tslint:disable-next-line:typedef
      valueGetter(params) {
        function getAssetName(assetName: string): string{
          const nameArray  = assetName.split('-');
          const name = nameArray[0].split('.');
          return name[1] === undefined ? name.toString() : name[1].toString();
        }
        return getAssetName(params.data.pool);
      },
      initialPinned: 'left',
      width: 110,
    },
    {
      headerName: 'LPvsHODL',
      field: 'lpvshodl',
      valueFormatter: this.forLPVSHODL,
      width: 130,
      unSortIcon: true,
      sortable: true,
    },

    { headerName: 'Profit $', field: 'profit', sortable: true, unSortIcon: true,
      comparator: this.numberCompare,
      valueFormatter: this.forProfit,
      initialWidth: 130,
    },
    { headerName: 'Current $', field: 'totalshareusd', sortable: true, unSortIcon: true,
      comparator: this.numberCompare,
      valueFormatter: this.forTotalShare,
      initialWidth: 140,
    },
    { headerName: 'Added $', field: 'totalstakeusd', sortable: true, unSortIcon: true,
      comparator: this.numberCompare,
      valueFormatter: this.forTotalStake,
      initialWidth: 140,
    },
    { headerName: 'Withdrawn $', field: 'totalunstakeusd', sortable: true, unSortIcon: true,
      comparator: this.numberCompare,
      valueFormatter: this.forTotalUnstak,
      initialWidth: 140,
    },
    { headerName: 'Days', field: 'days', initialWidth: 90, sortable: true, unSortIcon: true, cellStyle: { textAlign: 'center' } },
    {
      headerName: 'Period-based APY',
      field: 'lpvshodl',
      valueFormatter: this.calculateAPYNew,
      width: 200,
    },
    {  field: 'address', filter: true, initialWidth: 400 },

    {
      headerName: '🏆',
      headerClass: 'text-center',
      valueGetter(params): string {
        return params.data.rank_number;
      },
      sortable: true,
      initialPinned: 'right',
      initialWidth: 60,
      cellStyle: { textAlign: 'center' },
    }
  ];

  rowData: any;
  height = '';
  date = new Date();
  loadedTime = '';
  totalValue = '';
  userCount = '';
  poolAverage: PoolRank[] = []

  formatNumber: (params) => {
    'fgfg';
  };



  // tslint:disable-next-line:typedef
  ngOnInit() {
    this.loadData();
  }

  // tslint:disable-next-line:typedef
  loadData(){
    this.http.get(environment.ASGARD_BASE_URL + '/api/v1/ranking')
      .subscribe((data) => {
      //  console.log(data);
        this.height = data[0].height.toLocaleString('en-us', {minimumFractionDigits: 0});
        // tslint:disable-next-line:radix
        this.rowData = data;
        this.date = new Date(data[0].lastest * 1000);
        this.loadedTime = moment(this.date).fromNow();
        //   this.date = new Date(data[0].lastest);

      }, error => {
        console.log(error);
      });

    this.http.get(environment.ASGARD_BASE_URL + '/api/v1/ranking/total').subscribe((data) => {
    //  console.log('$' + data[0].totalshare.toFixed(2));
      this.totalValue = '$' + data[0].totalshare.toLocaleString('en-us', {minimumFractionDigits: 2});
      this.userCount = data[0].usercount.toLocaleString('en-us', {minimumFractionDigits: 0});
    });
    // http://localhost:3000/


    this.http.get<PoolRank[]>(environment.ASGARD_BASE_URL + '/api/v1/ranking/average').subscribe((data) => {
     // console.log(data)
      this.poolAverage = data
    });


  }

  forLPVSHODL(params): string {
    const value = parseFloat(params.data.lpvshodl);
    return (value > 0 ? '+' : '') + value.toFixed(2) + '%';
  }

  formatLPVsHODl(pool: PoolRank): string{
    const data = {
      data : {
        lpvshodl : pool.lpvshodl
      }
    }
   // console.log(data)
    return this.forLPVSHODL(data) ;
  }


  formatCurrency(value): string{
    return currencyFormatter.format(value, { locale: 'en-US' });
  }

  forTotalStake(params): string {
    const value = params.data.totalstakeusd;
    return  currencyFormatter.format(value, { locale: 'en-US' });
  }

  forProfit(params): string {
    const value = params.data.profit;
    return (value > 0 ? '+' : '') + currencyFormatter.format(value, { locale: 'en-US' });
  }

  forTotalUnstak(params): string {
    const value = params.data.totalunstakeusd;
    return  currencyFormatter.format(value, { locale: 'en-US' });
  }

  forTotalShare(params): string{
    const value = params.data.totalshareusd;
    return  currencyFormatter.format(value, { locale: 'en-US' });
  }

  //  APY = (1 + LP vs HODL % / (total days / 7 day per week)) ^ 52 -1
  calculateAPY(params): string {
    const dayCount = params.data.days;
    if (dayCount < 7) {
      return 'N/A';
    }
    const percentageNumber = parseFloat(params.data.lpvshodl);
    const numberOfWeek = dayCount / 7 ;
    const periodicRatePercent = percentageNumber / numberOfWeek;
    const periodicRate = periodicRatePercent / 100;
    //   console.log(periodicRate + '  '  + periodicRatePercent);
    const apy = ((Math.pow((periodicRate + 1), 52)) - 1) * 100;
    //  console.log(periodicRate + '  '  + periodicRatePercent + '  ' + apy.toFixed(2) + '%');
    return apy.toLocaleString('en-us', {maximumFractionDigits: 2}) + '%';
  }

  computAPYAvg(pool: PoolRank){
    const data = {
      data : {
        days : pool.days,
        lpvshodl : pool.lpvshodl
      }
    }
   // console.log(data)
    return this.calculateAPYNew(data)
  }

  calculateAPYNew(params): string {
    const dayCount = params.data.days;
    if (dayCount < 7) {
      return 'N/A';
    }

    const percentageNumber = parseFloat(params.data.lpvshodl);
    let dayLimit = 0; // dayCount >= 14 ? 14 : 7;
    let maxLimit = 0;
    let base = 0;

    if (dayCount >= 30) {
      dayLimit = 30;
      maxLimit = 12;
      base = 30;
    }else if (dayCount >= 14) {
      dayLimit = 14;
      maxLimit = 52;
      base = 7;
    }else {
      dayLimit = 7;
      maxLimit = 52;
      base = 7;
    }
    const numberOfWeek = dayCount / dayLimit ;
    const periodicRatePercent = percentageNumber / numberOfWeek;
    const periodicRate = periodicRatePercent / 100;
    //  console.log(periodicRate + '  '  + periodicRatePercent);
    const apy = ((Math.pow((periodicRate + 1), (maxLimit / (dayLimit / base)))) - 1) * 100;
    //   console.log(periodicRate + '  '  + periodicRatePercent + '  ' + apy.toFixed(2) + '%');
    return apy.toLocaleString('en-us', {maximumFractionDigits: 2}) + '%';
  }

  numberCompare(date1, date2): number{
    const value1 = parseFloat(date1);
    const value2 = parseFloat(date2);
    return  value1 - value2;
  }

  // tslint:disable-next-line:typedef
  onSelectionChanged(event) {
    const selectedRow = this.gridApi.getSelectedRows()[0];
    //  console.log(selectedRow);
    if (selectedRow !== undefined) {
      window.open('https://app.runestake.info/home?address=' + selectedRow.address, '_blank');
    }

    // document.querySelector('#selectedRows').innerHTML =
    //   selectedRows.length === 1 ? selectedRows[0].athlete : '';
  }

   getAssetName(assetName: string): string{
    const nameArray  = assetName.split('-');
    const name = nameArray[0].split('.');
    return name[1] === undefined ? name.toString() : name[1].toString();
  }

  // tslint:disable-next-line:typedef
  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
  }

}
