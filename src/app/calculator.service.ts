import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CalculatorService {

  constructor() { }

  calculateAPYNew(daycount: number, lpvshodl: number ): string {
    const dayCount = daycount;
    if (dayCount < 7) {
      return 'N/A';
    }

    const percentageNumber = lpvshodl;
    let dayLimit = 0; // dayCount >= 14 ? 14 : 7;
    let maxLimit = 0;
    let base = 0;

    if (dayCount >= 30) {
      dayLimit = 30;
      maxLimit = 12;
      base = 30;
    }else if (dayCount >= 14) {
      dayLimit = 14;
      maxLimit = 52;
      base = 7;
    }else {
      dayLimit = 7;
      maxLimit = 52;
      base = 7;
    }
    const numberOfWeek = dayCount / dayLimit ;
    const periodicRatePercent = percentageNumber / numberOfWeek;
    const periodicRate = periodicRatePercent / 100;
    //  console.log(periodicRate + '  '  + periodicRatePercent);
    const apy = ((Math.pow((periodicRate + 1), (maxLimit / (dayLimit / base)))) - 1) * 100;
    //   console.log(periodicRate + '  '  + periodicRatePercent + '  ' + apy.toFixed(2) + '%');
    return apy.toLocaleString('en-us', {minimumFractionDigits: 2}) + '%';
  }
}
